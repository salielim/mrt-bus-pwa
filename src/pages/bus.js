import React, { Component } from 'react';
import Geolocation from "react-geolocation";
import axios from 'axios';

class Foo extends Component {
  getBusStops = () => {
    // Get Lat Lon
    navigator.geolocation.getCurrentPosition((location) => {
      const latNo = location.coords.latitude
      const lonNo = location.coords.longitude
      const latGpsSubStr = latNo.toString().substring(0, 4)
      const lonGpsSubStr = lonNo.toString().substring(0, 6)
      console.log("lat:", latNo);
      console.log("lon:", lonNo);

      // Get Bus Stops
      axios.get('https://busrouter.sg/data/2/bus-stops.json')
        .then(function (response) {
          const busStops = response.data;

          let i;
          for (i = 0; i < busStops.length; i++) {
            let latDataSubStr = busStops[i].lat.substring(0, 4)
            let lonDataSubStr = busStops[i].lng.substring(0, 6)

            if (latDataSubStr === latGpsSubStr && lonDataSubStr === lonGpsSubStr) {
              console.log(busStops[i].name + " " + busStops[i].no)
            }
          }
        })
        .catch(function (error) {
          console.log(error)
        })
    });
  }

  render() {
    return (
      <button onClick={this.getBusStops}>Get bus wait times</button>
    )
  }
}

export default () => (
  <div>
    <h2>Bus</h2>
    {/* <button onClick={getBusStops}>Get bus wait times</button> */}
    <Foo />
  </div>
)