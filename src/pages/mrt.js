import React from "react";
import mrtMap from "../images/mrtmap.jpg";

export default () => (
  <div>
    <h2>MRT</h2>
    <p>pinch to zoom in/out</p>
    <img src={mrtMap} alt="MRT Map" />
  </div>
);