import React from "react";
import Link from "gatsby-link";

export default ({ children }) => (
  <div style={{ margin: `0 auto`, maxWidth: 850, padding: `0rem 1rem` }}>
    <header style={{ marginBottom: `1.5rem` }}>
      <Link to="/" style={{ textShadow: `none`, backgroundImage: `none` }}>
        <h1>MRT & Bus SG </h1>
      </Link>

      <div style={{ fontSize: `29px` }}>
        <Link to="/mrt/">🚆MRT</Link>&nbsp;&nbsp;
        <Link to="/bus/">🚍Bus</Link>&nbsp;&nbsp;
        <Link to="/">🔥Latest</Link>&nbsp;&nbsp;<br />
        <Link to="/manifest.json">Add icon to mobile 📱</Link>&nbsp;
      </div>
    </header>
    {children()}
    <Link to="/faq/">FAQ</Link>
  </div>
);