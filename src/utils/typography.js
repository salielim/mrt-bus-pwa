import Typography from "typography";
import oceanBeachTheme from "typography-theme-ocean-beach";
oceanBeachTheme.baseFontSize = "20px";
oceanBeachTheme.scaleRatio = 2.8;

const typography = new Typography(oceanBeachTheme);

export default typography;